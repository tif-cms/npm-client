import { Endpoints } from "../src/constant";
import transport from "../src/transport";

import CollectionSchema from "./constants/collection_schema";

export async function mochaGlobalSetup(): Promise<void> {
  /**
   * Create an App
   */
  const response = await transport.post(Endpoints.v1.App, {
    name: {
      en: "NPM Test App",
    },
  });
  if (response) {
    const { data } = response;
    const app_id = data.content.data._id;
    console.log(`App created: ${app_id}`);
    if (data.status) {
      process.env.CMS_APP_ID = app_id;
      process.env.CMS_API_KEY = app_id;
      for (const collection of Object.keys(CollectionSchema)) {
        await transport.post(`${Endpoints.v1.Collection}/add`, {
          ...CollectionSchema[collection],
          app_id,
        });
      }
    }
  } else {
    throw new Error("Unable to create an App.");
  }
}

export async function mochaGlobalTeardown(): Promise<void> {
  /**
   * Remove the App
   */
  const response = await transport.delete(
    `${Endpoints.v1.App}/${process.env.CMS_APP_ID}`
  );
  if (response) {
    console.log(`App removed: ${process.env.CMS_APP_ID}`);
    process.exit(0);
  } else {
    throw new Error("Unable to remove the App.");
  }
}
