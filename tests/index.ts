import { expect } from "chai";

import { CMS } from "../src";

const appId = process.env.CMS_APP_ID;
const apiKey = process.env.CMS_API_KEY;

describe("CMS", () => {
  let cms: CMS;

  before(() => {
    cms = new CMS({
      appId,
      apiKey,
    });
  });

  it("should have a Content resource", () => {
    expect(cms.Content).to.exist;
  });
});
