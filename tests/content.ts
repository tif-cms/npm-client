import { expect } from "chai";

import { CMS } from "../src";

const appId = process.env.CMS_APP_ID;
const apiKey = process.env.CMS_API_KEY;

describe("CMS", () => {
  let cms: CMS;

  before(() => {
    cms = new CMS({
      appId,
      apiKey,
    });
  });

  /**
   * query
   *
   * Positive Cases
   * - Query a collection
   * - Query a collection with a version
   * - Query a collection with fields
   * - Query a collection with sort
   * - Query a collection with filter
   * - Query a collection with pagination
   * - Query a collection with search
   *
   * Negative Cases
   * - Query a collection with an invalid version
   *
   */
  describe("query", () => {});

  /**
   * create
   *
   * Positive Cases
   * - Create a content
   * - Create a content with a version
   *
   * Negative Cases
   * - Create a content with an invalid version
   * - Create a content with an invalid collection
   * - Create a content with an invalid data
   * - Create a content with no data
   */
});
