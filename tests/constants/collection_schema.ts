export default {
  chapter: {
    key: "chapter",
    description: {
      en: "",
    },
    name: {
      en: "Chapter",
    },
    primary: "",
    created_at: "2022-01-15T17:19:50.053Z",
    created_by: null,
    updated_at: "2022-01-15T17:41:14.811Z",
    updated_by: null,
    sections: [
      {
        label: {
          en: "",
        },
        helper_text: {
          en: "",
        },

        created_at: "2022-01-15T17:19:50.071Z",
        created_by: null,
        updated_at: "2022-01-15T17:41:14.828Z",
        updated_by: null,
        fields: [
          {
            key: "name",
            helper_text: {
              en: "",
            },
            label: {
              en: "Name",
            },
            kind: "string",
            field_interface: "TextWidget",
            options: {},
            default_value: "",
            fields: [],
            created_at: "2022-01-15T17:19:50.087Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.833Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "lead",
            helper_text: {
              en: "",
            },
            label: {
              en: "Lead",
            },
            kind: "string",
            field_interface: "TextWidget",
            options: {},
            default_value: "",
            fields: [],
            created_at: "2022-01-15T17:19:50.102Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.837Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "gender",
            helper_text: {
              en: "",
            },
            label: {
              en: "Gender",
            },
            kind: "string",
            field_interface: "RadioWidget",
            options: {
              dropDownOptions: ["Male", "Female", "Other"],
            },
            default_value: "",
            fields: [],
            created_at: "2022-01-15T17:19:50.106Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.840Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "city",
            helper_text: {
              en: "",
            },
            label: {
              en: "City",
            },
            kind: "array-string",
            field_interface: "DropdownWidget",
            options: {
              dropDownOptions: ["Bhopal", "Mumbai", "Vapi", "Bhatisuda"],
            },
            default_value: "",
            fields: [],
            created_at: "2022-01-15T17:19:50.113Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.844Z",
            updated_by: null,
            rules: [],
          },
        ],
      },
      {
        label: {
          en: "Timestamps",
        },
        helper_text: null,

        created_at: "2022-01-15T17:19:50.117Z",
        created_by: null,
        updated_at: "2022-01-15T17:41:14.852Z",
        updated_by: null,
        fields: [
          {
            key: "created_at",
            helper_text: {
              en: "",
            },
            label: {
              en: "Created At",
            },
            kind: "date",
            field_interface: "DateTimeWidget",
            options: {},
            default_value: null,
            fields: [],
            created_at: "2022-01-15T17:19:50.124Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.857Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "updated_at",
            helper_text: {
              en: "",
            },
            label: {
              en: "Updated At",
            },
            kind: "date",
            field_interface: "DateTimeWidget",
            options: {},
            default_value: null,
            fields: [],
            created_at: "2022-01-15T17:19:50.129Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.861Z",
            updated_by: null,
            rules: [],
          },
        ],
      },
      {
        label: {
          en: "Other",
        },
        helper_text: {
          en: "",
        },

        created_at: "2022-01-15T17:41:14.865Z",
        created_by: null,
        updated_at: "2022-01-15T17:41:14.865Z",
        updated_by: null,
        fields: [
          {
            key: "startups",
            helper_text: {
              en: "",
            },
            label: {
              en: "Startups",
            },
            kind: "group",
            field_interface: "ArrayWidget",
            options: {},
            default_value: "",
            fields: [
              {
                key: "name",
                label: {
                  en: "Name",
                },
                helper_text: {
                  en: "",
                },
                kind: "string",
                field_interface: "TextWidget",
                default_value: "",
                rules: [],
                options: {},
                fields: [],
              },
              {
                key: "link",
                label: {
                  en: "Link",
                },
                helper_text: {
                  en: "",
                },
                kind: "string",
                field_interface: "URLWidget",
                default_value: "",
                rules: [],
                options: {},
                fields: [],
              },
            ],
            created_at: "2022-01-15T17:41:14.877Z",
            created_by: null,
            updated_at: "2022-01-15T17:41:14.877Z",
            updated_by: null,
            rules: [],
          },
        ],
      },
    ],
  },
  event: {
    key: "event",

    description: {
      en: "",
    },
    name: {
      en: "Event",
    },
    primary: "",
    created_at: "2022-01-16T06:07:32.657Z",
    created_by: null,
    updated_at: "2022-01-16T06:07:32.657Z",
    updated_by: null,
    sections: [
      {
        label: {
          en: "",
        },
        helper_text: {
          en: "",
        },

        created_at: "2022-01-16T06:07:32.667Z",
        created_by: null,
        updated_at: "2022-01-16T06:07:32.667Z",
        updated_by: null,
        fields: [
          {
            key: "title",
            helper_text: {
              en: "",
            },
            label: {
              en: "Title",
            },
            kind: "string",
            field_interface: "TextWidget",
            options: {},
            default_value: "",
            fields: [],
            created_at: "2022-01-16T06:07:32.675Z",
            created_by: null,
            updated_at: "2022-01-16T06:07:32.675Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "description",
            helper_text: {
              en: "",
            },
            label: {
              en: "Description",
            },
            kind: "string",
            field_interface: "TextWidget",
            options: {},
            default_value: "",
            fields: [],
            created_at: "2022-01-16T06:07:32.680Z",
            created_by: null,
            updated_at: "2022-01-16T06:07:32.680Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "event_date",
            helper_text: {
              en: "",
            },
            label: {
              en: "Event Date",
            },
            kind: "date",
            field_interface: "DateWidget",
            options: {},
            default_value: "",
            fields: [],
            created_at: "2022-01-16T06:07:32.689Z",
            created_by: null,
            updated_at: "2022-01-16T06:07:32.689Z",
            updated_by: null,
            rules: [],
          },
        ],
      },
      {
        label: {
          en: "Timestamps",
        },
        helper_text: null,

        created_at: "2022-01-16T06:07:32.691Z",
        created_by: null,
        updated_at: "2022-01-16T06:07:32.691Z",
        updated_by: null,
        fields: [
          {
            key: "created_at",
            helper_text: {
              en: "",
            },
            label: {
              en: "Created At",
            },
            kind: "date",
            field_interface: "DateTimeWidget",
            options: {},
            default_value: null,
            fields: [],
            created_at: "2022-01-16T06:07:32.695Z",
            created_by: null,
            updated_at: "2022-01-16T06:07:32.695Z",
            updated_by: null,
            rules: [],
          },
          {
            key: "updated_at",
            helper_text: {
              en: "",
            },
            label: {
              en: "Updated At",
            },
            kind: "date",
            field_interface: "DateTimeWidget",
            options: {},
            default_value: null,
            fields: [],
            created_at: "2022-01-16T06:07:32.698Z",
            created_by: null,
            updated_at: "2022-01-16T06:07:32.698Z",
            updated_by: null,
            rules: [],
          },
        ],
      },
    ],
  },
};
