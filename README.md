[![The Internet Folks Logo](https://theinternetfolks.com/assets/images/logo.png)](https://theinternetfolks.com)

# CMS Client

Library client to help you interact with TIF CMS's API in a developer friendly manner.

## Installation

Install with npm

```bash
  npm install @theinternetfolks/cms-client
```

Install with yarn

```bash
  yarn add @theinternetfolks/cms-client
```

## Usage/Examples

```javascript
import Component from "my-project";

function App() {
  return <Component />;
}
```

## Documentation

You can access the full documentation [here](https://linktodocumentation).

## Support

For support, email hi [at] theinternetfolks.com.

## License

[MIT](https://choosealicense.com/licenses/mit/)
