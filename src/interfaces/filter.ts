export type Filter = LogicalFilter | FieldFilter;

export enum LogicalOptions {
  _and = '_and',
  _or = '_or'
}

export type LogicalFilter = {
  [key in LogicalOptions]: Filter[];
};

export type NestedFieldFilter =
  | FieldFilterOperator
  | FieldValidationOperator
  | FieldFilter;

export type FieldFilter = {
  [field: string]: NestedFieldFilter;
};

export type FieldFilterOperator = {
  _eq?: string | number | boolean;
  _neq?: string | number | boolean;
  _lt?: string | number;
  _lte?: string | number;
  _gt?: string | number;
  _gte?: string | number;
  _in?: (string | number)[];
  _nin?: (string | number)[];
  _contains?: string;
  _ncontains?: string;
  _starts_with?: string;
  _nstarts_with?: string;
  _ends_with?: string;
  _nends_with?: string;
  _empty?: boolean;
  _nempty?: boolean;
  _between?: [string | number, string | number];
  _nbetween?: [string | number, string | number];
};

export type FieldValidationOperator = {
  _submitted?: boolean;
  _regex?: string;
};
