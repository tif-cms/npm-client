import { CMSClient } from "./client";
import { Endpoints, ErrorCodes } from "./constant";
import { Query } from "./interfaces/query";

interface ICMSContentQueryOptions {
  params?: Query;
  version?: string;
}

export class CMSContent {
  constructor(private client: CMSClient) {
    //
  }

  public async query(
    collection: string,
    options: ICMSContentQueryOptions = {}
  ): Promise<any> {
    const version = options.version ?? "v1";
    const endpoints = Endpoints[version];

    if (!endpoints) {
      throw Error(ErrorCodes[1503]);
    }

    return this.client.get(
      `${endpoints.Content}/${collection}`,
      options.params
    );
  }

  public async create(data: Record<string, unknown>): Promise<any> {
    //
  }
}
