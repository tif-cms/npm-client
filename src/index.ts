import { CMSContent } from "./content";
import { CMSClient, CMSClientConfig } from "./client";

const resources = {
  Content: CMSContent,
};

export class CMS {
  Content: CMSContent;

  constructor(public config: CMSClientConfig) {
    const client = new CMSClient(config);
    for (const resourceName in resources) {
      if (resources.hasOwnProperty(resourceName)) {
        this[resourceName] = new resources[resourceName](client);
      }
    }
  }
}
