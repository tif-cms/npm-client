import { AxiosRequestConfig, AxiosRequestHeaders, Method } from "axios";
import transport from "./transport";

/**
 * @interface CMSClientConfig
 */
export interface CMSClientConfig {
  /**
   * The base App Id of your CMS.
   */
  appId: string;
  /**
   * The Api Key for your CMS Account.
   */
  apiKey: string;
}

/**
 * The CMS Client.
 * The client helps to interact with the CMS, using API calls.
 * @param {CMSClientConfig} config The configuration for the client.
 * @param {string} config.appId The base App Id of your CMS.
 * @param {string} config.apiKey The Api Key for your CMS Account.
 *
 */
export class CMSClient {
  constructor(public config?: CMSClientConfig) {}

  /**
   * Method to call the GET method on the CMS.
   * @param {string} url - The url to call.
   * @param {object} [params] - The query params to pass to the url.
   */
  public async get(url: string, params?: any): Promise<any> {
    return this.request("GET", url, params);
  }

  /**
   * Method to call the POST method on the CMS.
   * @param {string} url - The url to call.
   * @param {object} data - The data to pass to the url.
   */
  public async post(url: string, data: any): Promise<any> {
    return this.request("POST", url, data);
  }

  /**
   * Method to call the PUT method on the CMS.
   * @param {string} url - The url to call.
   * @param {object} data - The data to pass to the url.
   */
  public async put(url: string, data: any): Promise<any> {
    return this.request("PUT", url, data);
  }

  /**
   * Method to call the DELETE method on the CMS.
   * @param {string} url - The url to call.
   * @param {object} [params] - The query params to pass to the url.
   */
  public async delete(url: string, params?: any): Promise<any> {
    return this.request("DELETE", url, params);
  }

  /**
   * Method to request an HTTP endpoint with a method on the CMS.
   * @param {string} method - The HTTP method to call.
   * @param {string} url - The url to call.
   * @param {object} [params] - The query params or data to pass to the url.
   */
  public async request(
    method: Method,
    url: string,
    params?: any
  ): Promise<any> {
    const headers: AxiosRequestHeaders = {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${this.config.apiKey}`,
      "X-CMS-App-Id": this.config.appId,
    };

    const options: AxiosRequestConfig = {
      url,
      method,
      headers,
      timeout: 30e3,
    };

    if (params) {
      if (method === "GET") {
        options.params = params;
      } else {
        options.data = params;
      }
    }

    const response = await transport.request(options);

    return response;
  }
}
