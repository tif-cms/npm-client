const Version = ["v1"] as const;

const Resources = ["App", "Content", "Collection", "Section"] as const;

export const Endpoints: Record<
  typeof Version[number],
  Record<typeof Resources[number], string>
> = {
  v1: {
    App: "/v1/app",
    Content: "/v1/content",
    Collection: "/v1/schema/collection",
    Section: "/v1/schema/section",
  },
};

export const ErrorCodes = {
  1501: "Error 1501: The app id is invalid.",
  1502: "Error 1502: The api key is invalid.",
  1503: "Error 1503: The version is invalid.",
};
