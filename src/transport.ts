import axios from "axios";

const transport = axios.create({
  baseURL: process.env.CMS_BASE_URL,
});

export default transport;
